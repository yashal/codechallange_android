package com.example.yash.demoproject.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yash.demoproject.activity.MainActivity;
import com.example.yash.demoproject.R;
import com.example.yash.demoproject.pojos.ResultPojo;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Yash on 22/9/2017.
 */
public class SongListAdapter extends RecyclerView.Adapter<SongListAdapter.ViewHolder> {

    private MainActivity context;
    private ArrayList<ResultPojo> resultdata;

    public SongListAdapter(MainActivity context, ArrayList<ResultPojo> resultdata) {
        this.context = context;
        this.resultdata = resultdata;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.song_row_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ResultPojo data = resultdata.get(position);
        context.setImageInLayout(context, 90,90, data.getArtworkUrl100(), holder.artistImage);
        if (data.getArtistName() != null && !data.getArtistName().isEmpty())
        {
            holder.artistName.setText(data.getArtistName());
        }
        if (data.getCollectionName() != null && !data.getCollectionName().isEmpty())
        {
            holder.collectionName.setText(data.getCollectionName());
        }
        if (data.getTrackName() != null && !data.getTrackName().isEmpty())
        {
            holder.trackName.setText(data.getTrackName());
        }
        holder.price.setText("Collection Price: "+data.getCollectionPrice()+" "+data.getCurrency());
        holder.itemView.setTag(R.string.key, data);
        holder.itemView.setOnClickListener((MainActivity) context);
    }

    @Override
    public int getItemCount() {
        return resultdata.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView artistName, collectionName, trackName, price;
        private ImageView artistImage;

        public ViewHolder(View itemView) {
            super(itemView);
            artistName = (TextView) itemView.findViewById(R.id.artistName);
            collectionName = (TextView) itemView.findViewById(R.id.collectionName);
            trackName = (TextView) itemView.findViewById(R.id.trackName);
            price = (TextView) itemView.findViewById(R.id.price);
            artistImage = (ImageView) itemView.findViewById(R.id.artistImage);
        }
    }
}
