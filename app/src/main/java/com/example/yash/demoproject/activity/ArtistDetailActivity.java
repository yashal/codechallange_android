package com.example.yash.demoproject.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yash.demoproject.R;
import com.squareup.picasso.Picasso;

public class ArtistDetailActivity extends AppCompatActivity {

    private ArtistDetailActivity ctx = this;
    private ImageView artistImage;
    private TextView header, trackName, artistName, collectionName, trackCensoredName, collectionPrice, trackPrice, countryname,
            primaryGenre_Name;
    private String track_name = "";
    private String artist_name = "";
    private String collection_name = "";
    private String track_censor = "";
    private float track_price = 0.0f;
    private float collection_price = 0.0f;
    private String currency = "";
    private String country = "";
    private String primaryGenreName = "";
    private String artworkUrl10 = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_detail);

        init();

        header.setText(track_name);
        trackName.setText(track_name);
        artistName.setText(artist_name);
        collectionName.setText(collection_name);
        trackCensoredName.setText(track_censor);
        countryname.setText(country);
        primaryGenre_Name.setText(primaryGenreName);
        trackPrice.setText(track_price+" "+currency);
        collectionPrice.setText(collection_price+" "+currency);
        setImageInLayout(ctx, 90,90, artworkUrl10, artistImage);
    }
    private void init()
    {
        artistImage = (ImageView)findViewById(R.id.artistImage);
        header = (TextView) findViewById(R.id.header);
        trackName = (TextView) findViewById(R.id.trackName);
        artistName = (TextView) findViewById(R.id.artistName);
        collectionName = (TextView) findViewById(R.id.collectionName);
        trackCensoredName = (TextView) findViewById(R.id.trackCensoredName);
        collectionPrice = (TextView) findViewById(R.id.collectionPrice);
        trackPrice = (TextView) findViewById(R.id.trackPrice);
        countryname = (TextView) findViewById(R.id.country);
        primaryGenre_Name = (TextView) findViewById(R.id.primaryGenreName);

        if (getIntent().getStringExtra("track_name")!= null)
        {
            track_name = getIntent().getStringExtra("track_name");
        }

        if (getIntent().getStringExtra("artist_name")!= null)
        {
            artist_name = getIntent().getStringExtra("artist_name");
        }

        if (getIntent().getStringExtra("collection_name")!= null)
        {
            collection_name = getIntent().getStringExtra("collection_name");
        }

        if (getIntent().getStringExtra("track_censor")!= null)
        {
            track_censor = getIntent().getStringExtra("track_censor");
        }

        if (getIntent().getStringExtra("currency")!= null)
        {
            currency = getIntent().getStringExtra("currency");
        }

        if (getIntent().getStringExtra("country")!= null)
        {
            country = getIntent().getStringExtra("country");
        }

        if (getIntent().getStringExtra("artworkUrl10")!= null)
        {
            artworkUrl10 = getIntent().getStringExtra("artworkUrl10");
        }

        if (getIntent().getStringExtra("primaryGenreName")!= null)
        {
            primaryGenreName = getIntent().getStringExtra("primaryGenreName");
        }
        track_price = getIntent().getFloatExtra("track_price", 0.0f);
        collection_price = getIntent().getFloatExtra("collection_price", 0.0f);

    }
    public void setImageInLayout(Context ctx, int width, int height, String url, ImageView image) {
        Picasso.with(ctx).load(url).resize(width, height).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(image);
    }
}
