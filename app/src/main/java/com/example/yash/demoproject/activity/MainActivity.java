package com.example.yash.demoproject.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.yash.demoproject.pojos.ResultPojo;
import com.example.yash.demoproject.utils.AppDialogs;
import com.example.yash.demoproject.utils.ConnectionDetector;
import com.example.yash.demoproject.R;
import com.example.yash.demoproject.adapter.SongListAdapter;
import com.example.yash.demoproject.model.ApiClient;
import com.example.yash.demoproject.model.ApiInterface;
import com.example.yash.demoproject.pojos.SearchDatapojo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private MainActivity ctx = this;
    private ArrayList<HashMap<String, String>> resultList;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ConnectionDetector cd;
    private LinearLayout no_item_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cd = new ConnectionDetector(getApplicationContext());
        recyclerView = (RecyclerView) findViewById(R.id.song_list_recycler_view);
        no_item_layout = (LinearLayout) findViewById(R.id.no_item_layout);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        getSongList();


    }

    public void setImageInLayout(Context ctx, int width, int height, String url, ImageView image) {
        Picasso.with(ctx).load(url).resize(width, height).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(image);
    }

    private void getSongList() {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = AppDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<SearchDatapojo> call = apiService.serachSong("Michael+jackson");
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<SearchDatapojo>() {
                @Override
                public void onResponse(Call<SearchDatapojo> call, Response<SearchDatapojo> response) {
                    System.out.println("hh Success: ");

                    if (response != null && response.body() != null) {
                        if (response.body().getResults() != null && response.body().getResults().size() > 0) {
                            mAdapter = new SongListAdapter(ctx, response.body().getResults());
                            recyclerView.setAdapter(mAdapter);
                            no_item_layout.setVisibility(View.GONE);
                        }
                        else
                        {
                            no_item_layout.setVisibility(View.VISIBLE);
                        }

                    } else {
                        Toast.makeText(ctx, "Server not responding", Toast.LENGTH_SHORT).show();
                        no_item_layout.setVisibility(View.VISIBLE);

                    }
                    d.dismiss();
                }

                @Override
                public void onFailure(Call<SearchDatapojo> call, Throwable t) {
                    // Log error here since request failed
                    System.out.println("hh failure: " + t.getMessage());
                    d.dismiss();
                    Toast.makeText(ctx, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(ctx, "No internet Connection", Toast.LENGTH_SHORT).show();
            no_item_layout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {

        ResultPojo data = (ResultPojo)view.getTag(R.string.key);
        Intent intent = new Intent(ctx, ArtistDetailActivity.class);
        intent.putExtra("track_name", data.getTrackName());
        intent.putExtra("artist_name", data.getArtistName());
        intent.putExtra("collection_name", data.getCollectionName());
        intent.putExtra("track_censor", data.getTrackCensoredName());
        intent.putExtra("track_price", data.getTrackPrice());
        intent.putExtra("collection_price", data.getCollectionPrice());
        intent.putExtra("currency", data.getCurrency());
        intent.putExtra("country", data.getCountry());
        intent.putExtra("primaryGenreName", data.getPrimaryGenreName());
        intent.putExtra("artworkUrl10", data.getArtworkUrl100());
        startActivity(intent);

    }
}
