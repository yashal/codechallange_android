package com.example.yash.demoproject.pojos;

import java.util.ArrayList;

/**
 * Created by Yash on 22/9/2017.
 */

public class SearchDatapojo {

    private int resultCount;
    private ArrayList<ResultPojo> results;

    public int getResultCount() {
        return resultCount;
    }

    public void setResultCount(int resultCount) {
        this.resultCount = resultCount;
    }

    public ArrayList<ResultPojo> getResults() {
        return results;
    }

    public void setResults(ArrayList<ResultPojo> results) {
        this.results = results;
    }
}
