package com.example.yash.demoproject.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class AppDialogs {

    private Activity ctx;
    private Dialog lDialog;
    private boolean closeActivity = true;

    public static ProgressDialog showLoading(Activity activity) {
        ProgressBar  progressBar = new ProgressBar(activity, null, android.R.attr.progressBarStyleSmall);
        ProgressDialog mProgressDialog = new ProgressDialog(activity);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        if (!activity.isFinishing() && !mProgressDialog.isShowing())
            mProgressDialog.show();
        return mProgressDialog;
    }

    public AppDialogs(Activity ctx) {
        this.ctx = ctx;
    }

}
